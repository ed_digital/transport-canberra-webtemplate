// Fix iOS scaling bug. http://webdesignerwall.com/tutorials/iphone-safari-viewport-scaling-bug
(function(doc) {
  var addEvent = 'addEventListener',
      type = 'gesturestart',
      qsa = 'querySelectorAll',
      scales = [1, 1],
      meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];
  function fix() {
    meta.content = 'width=device-width,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
    doc.removeEventListener(type, fix, true);
  }

  if ((meta = meta[meta.length - 1]) && addEvent in doc) {
    fix();
    scales = [0.25, 1.6];
    doc[addEvent](type, fix, true);
  }

  // Fix iOS displaying URL bar. http://remysharp.com/2010/08/05/doing-it-right-skipping-the-iphone-url-bar
  function hideUrlBar() {
    /iPhone/.test(MBP.ua) && !pageYOffset && !location.hash && setTimeout(function () {
      window.scrollTo(0, 1);
    }, 1000);
  }
}(document));

// Accessible navigation - add .hover to list items on keyboard navigation.
$.fn.accessHover = function () {
  var el = $(this);
  //Setup dropdown menus for IE6.
  $("li", el).mouseover(function() {
    $(this).addClass("hover");
  }).mouseout(function() {
    $(this).removeClass("hover");
  });
  
  // Make dropdown menus keyboard accessible.
  $("a", el).focus(function() {
    $(this).parents("li").addClass("hover");
  }).blur(function() {
    $(this).parents("li").removeClass("hover");
  });
};

function collapseNavigation() {
  var toggleMenu = "<button class='toggleMenu'>&#9776; Menu<span>&#751;</span></button>";
  $("#navcontainer").hide().before(toggleMenu);
  $(".toggleMenu").click(function(){
    $(this).toggleClass("active");
    if ( $(this).hasClass("active") )
    {
      $("#navcontainer").show();
    }
    else
    {
      $("#navcontainer").hide();
    }
  });
}

function expandNavigation() {
  $("#navcontainer").show();
  $(".toggleMenu").remove();
}

$(document).ready(function() {

  $("#slideshowmodule").css({ 'overflow': 'hidden'});

  // Add classes psuedo-classes - <IE9 does not support :last-child.
  $("#siteinformationnav li:last-child").addClass("infolastchildborder");

  // Elements to add .hover to.
  $("#dropdownnav").accessHover();
  $("#megamenunav").accessHover();
  $("#asidenav").accessHover();

  // Clear placeholder text for site search box.
  var el = $("#searchString");
  el.focus(function(e) {
  if (e.target.value === e.target.defaultValue)
    e.target.value = '';
  });
  // Add in placeholder text if search is empty on blur.
  el.blur(function(e) {
  if (e.target.value === '')
    e.target.value = e.target.defaultValue;
  });

  // Media query JavaScript.
  var initial_load;
  $(window).bind('load resize', function(){
    if($(window).width() <= 767){
        if(initial_load === 0 || initial_load === undefined){ mQry.mobile(); }
    }else {
        if(initial_load === 1 || initial_load === undefined){ mQry.desktop(); }
    }
  });

  mQry = {
    mobile : function(){
      initial_load = 1;
      collapseNavigation();
      $("#emergencyalert img, #addthisbutton, #fatfooterblock, #dropdownnav li ul, #megamenunav #megamenunavcontent").addClass("display-none");
      $("#slideshowmodule").tabs("rotate", 0, false);
      $("#leftcolumn").remove().insertBefore($("#footercontainer"));
    },
    desktop : function(){
      initial_load = 0;
      expandNavigation();
      $("#emergencyalert img, #breadcrumbs, #addthisbutton, #fatfooterblock, #dropdownnav li ul, #megamenunav .megamenunavcontent").removeClass("display-none");
      $("#slideshowmodule").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
      $("#slideshowmodule").hover(function() { $("#slideshowmodule").tabs("rotate",0, true); },function() { $("#slideshowmodule").tabs("rotate", 5000,true); });
      $("#leftcolumn").remove().insertBefore($("#contentcolumn"));
    }
  };  

  //Add arrow for dropdown flyout where <span>'s can't be used to match sample DOM.
  $("#dropdownnav").children().children("li").children("ul").children("li").children("ul").prev("a").contents().wrap("<span>");

  // Fix for Webkit not applying focus to skip links
  $("#skip").on("click", function(){
    $('#main').attr('tabIndex',-1).focus();
  });

  var responsiveTables = $('.responsive-table-two-column');
  if (responsiveTables.length) {
      var rows = $('tr', responsiveTables);
      $('th', responsiveTables).each(function (i, header) {
          rows.each(function () {
              $('td:nth-child(' + (i+1) + ')', this).attr('data-header', $(header).text());
          });
      });
    }
});