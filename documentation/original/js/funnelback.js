// Query completion setup.
jQuery(function() {
    jQuery("#query, #query-global-search").fbcompletion({
        'enabled' : 'enabled',
        'collection' : 'act-gov',
        'program' : 'http://www.search.act.gov.au/s/suggest.json',
        'format' : 'simple',
        'alpha' : '.5',
        'show' : '10',
        'sort' : '0',
        'length' : '3',
        'delay' : '0',
        'profile' : '_default'
    });
});
$(document).ready(function(){
  $("#search-advanced form").hide();
  $("#search-advanced").on('click', '#search-advanced-toggle', function(e) {
    e.preventDefault();
    $("#search-advanced form").slideToggle('fast');
    $("#search-advanced-toggle").toggleClass('active');
  });
});