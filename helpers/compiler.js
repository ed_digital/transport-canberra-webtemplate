"use strict";

const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const browserify = require('browserify');
const watchify = require('watchify');

const autoprefixer = require('gulp-autoprefixer');

const gulpWatch = require('gulp-watch');
const less = require('gulp-less');

const swig = require('swig');
const swigExtras = require('swig-extras');
const glob = require('glob');

const path = require('path');

const babel = require('babelify');
const chalk = require('chalk');

const fse = require('fs-extra');

const async = require('async');

const EventEmitter = require('events').EventEmitter;

swig.setDefaults({
	loader: swig.loaders.fs(__dirname + '/../src/templates'),
	cache: false
});
swigExtras.useTag(swig, 'markdown');

class Compiler extends EventEmitter {
	
	constructor() {
		super();
		
		this.errors = {};
	}
	
	clearErrors(type) {
		this.errors[type] = null;
	}
	
	addError(type, title, msg) {
		if(!this.errors[type]) this.errors[type] = [];
		let err = {
			type: type,
			title: title,
			message: msg
		};
		this.errors[type].push(err);
		this.emit('compileError', err);
		this.changed();
	}
	
	getErrors() {
		let errs = [];
		for(let key in this.errors) {
			for(let index in this.errors[key]) {
				errs.push(this.errors[key][index]);
			}
		}
		return errs;
	}
	
	compile(watch) {
		this.copyResources(watch);
		this.compileLESS(watch);
		this.compileJS(watch);
		this.compileHTML(watch);
	}
	
	watch() {
		return this.compile(true);
	}
	
	copyResources(watch) {
	
		let directories = ['./src/img', './src/fonts'];
		
		let copyTimeout;
		let performCopy = () => {
			clearTimeout(copyTimeout);
			copyTimeout = setTimeout(() => {
				console.log(chalk.yellow(">> Copying resources"));
				this.clearErrors('resources');
				async.each(directories, (directory, next) => {
					let name = path.basename(directory);
					console.log("Got ",directory,name);
					fse.remove('./build/'+name, (err) => {
						if(err) {
							next(err);
						} else {
							fse.copy(directory, path.join('./build/', name), next);
						}
					});
				}, (err) => {
					if(err) {
						console.log(chalk.black(chalk.bgRed(">> Resource copying error")));
						console.log(err.message);
						this.addError('resources', 'An error occurred copying a resource to the build destination', err.message);
					} else {
						console.log(chalk.cyan(">> Finished copying resources"));
					}
					this.changed();
				});
			}, 500);
		};
		
		if(watch) {
			if(watch) {
				for(let directory of directories) {
					gulpWatch(path.join(directory, '**/*'), performCopy);
				}
			}
		}
		
		performCopy();
		
	}
	
	compileHTML(watch) {
		
		let compile = (file, callback) => {
			let template = swig.renderFile(file, {}, (err, result) => {
				if(err) {
					console.log(chalk.black(chalk.bgRed(">> Swig Compiler Error for "+file)));
					console.log(err.message);
					this.addError('swig', `Failed to compile ${file}`, err.message);
					callback(true);
				} else {
					console.log(chalk.cyan(">> Finished compiling "+file));
					fse.outputFile('./build/pages/'+path.basename(file).replace(/\.[a-z0-9]+$/i, '.html'), result, 'utf8', callback);
				}
			});
		};
		
		let compileAll = () => {
			this.clearErrors('swig');
			console.log(chalk.yellow(">> Compiling Swig templates"));
			async.series([
				(next) => {
					fse.remove('./build/pages', next);
				},
				(next) => {
					fse.mkdirs('./build/pages', next);
				},
				(next) => {
					glob(__dirname+'/../src/pages/*.swig', {}, (err, files) => {
						if(err) throw err;
						async.each(files, compile, next);
					});
				}
			], () => {
				this.changed();
			});
		};
		
		if(watch) {
			gulpWatch('./src/**/*.swig', () => {
				console.log(chalk.magenta("------- Detected changes to Swig templates -------"));
				compileAll();
			});
			gulpWatch('./*.md', () => {
				console.log(chalk.magenta("------- Detected changes to Swig templates -------"));
				compileAll();
			});
		}
		
		compileAll();
		
	}
	
	compileLESS(watch) {
		
		console.log(chalk.yellow(">> Compiling LESS"));
		
		let files = ['screen.less', 'print.less', 'documentation.less'];
		
		let compile = (file) => {
			
			gulp.src('./src/less/'+file)
				.pipe(less())
				.pipe(autoprefixer())
				.on('error', (err) => {
					
					console.log(chalk.black(chalk.bgRed(">> LESS Compiler Error")));
					console.log(err.message);
					
					this.addError('less', `Failed to compile ${file}`, err.message);
					
				})
				.pipe(gulp.dest('./build/css'))
				.on('end', () => {
					console.log(chalk.cyan(">> Finished compiling "+file));
					this.changed();
				});
			
		};
		
		let compileAll = () => {
			this.clearErrors('less');
			for(let file of files) {
				compile(file);
			}
		};
		
		if(watch) {
			gulpWatch('./src/less/**/*', () => {
				console.log(chalk.magenta("------- Detected changes in LESS -------"));
				compileAll();
			});
		}
		
		compileAll();
		
	}
	
	compileJS(watch) {
		var bundler = watchify(browserify('./src/js/index.js', { debug: false }).transform(babel.configure({
			presets: ['es2015']
		})));

		const rebundle = () => {
			console.log(chalk.yellow(">> Compiling JS"));
			this.clearErrors('js');
			
			bundler.bundle()
				.on('error', (err) => {
					
					console.log(chalk.black(chalk.bgRed(">> JS Compiler Error")));
					console.log(err.message + (err.codeFrame ? "\n"+err.codeFrame : ""));
					
					this.addError('js', `Failed to compile JS`, err.message + (err.codeFrame ? "\n"+err.codeFrame : ""));
					
				})
				.pipe(source('bundle.js'))
				.pipe(buffer())
				.pipe(sourcemaps.init({
					loadMaps: true
				}))
				.pipe(sourcemaps.write('./'))
				.pipe(gulp.dest('./build/js'));
		};

		if(watch) {
			bundler.on('update', () => {
				console.log(chalk.magenta("------- Detected changes in JS -------"));
				rebundle();
			});
			bundler.on('time', (time) => {
				console.log(chalk.cyan(">> JS compilation completed in "+time+"ms"));
				this.changed();
			});
		}

		rebundle();
	}
	
	changed() {
		clearTimeout(this._changeDebounce);
		this._changeDebounce = setTimeout(() => {
			this.emit('changed');
		}, 300);
	}
	
}

module.exports = Compiler;