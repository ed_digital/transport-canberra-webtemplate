const express = require('express');
const Compiler = require('./compiler');

const chalk = require('chalk');

class WebServer {
	
	constructor() {
		
		// Start express
		this.app = express();
		this.app.use('/devcheck', (req, res) => {
			this.compiler.once('changed', () => {
				res.end(JSON.stringify({
					refresh: true
				}));
			});
		});
		
		this.app.use('/pages/', (req, res, next) => {
			let compilerErrors = this.compiler.getErrors();
			if(compilerErrors.length > 0) {
				res.write("<div style='font-family: monospace; font-size: 14px; line-height: 1.5em'>");
				for(let err of compilerErrors) {
					res.write(`<div style='background-color: red; color: white; padding: 0px 5px;'>${err.type} error: ${err.title}</div>`);
					res.write(`<pre style='color: #444444; padding: 0px 5px;'>${err.message}</pre>`);
				}
				res.write("</div>");
				res.write("<script>setTimeout(function() { window.history.go(0) }, 2000)</script>");
				res.end();
			} else {
				next();
			}
		});
		
		this.app.use(express.static('./build'));
		
		let port = 2016;
		this.app.listen(port, () => {
			console.log(chalk.yellow(">> Listening on port "+port));
		});
		
		// Start compiler
		this.compiler = new Compiler();
		this.compiler.watch();
		
	}
	
}

module.exports = WebServer;