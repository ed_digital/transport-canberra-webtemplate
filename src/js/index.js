import "babel-polyfill";
import $ from 'jquery';
import _widget from 'jquery-ui/widget';
import _transit from 'jquery.transit';
import Site from './site';

// Fake Console
window.console = console || {
	log: () => {},
	error: () => {}
};

// Load jQuery and the Site instance into the window object
window.jQuery = window.$ = $;
window.Site = new Site();

// Load widgets
let plugins = [
	require('./widgets/tabs'),
	require('./widgets/trip-planner'),
	require('./widgets/autocomplete'),
	require('./widgets/slider'),
	require('./widgets/tc'),
	require('./widgets/timetables'),
	require('./widgets/mobile-nav'),
	require('./widgets/sidebar')
];
for(let plugin of plugins) {
	plugin(window.Site, window.jQuery);
}