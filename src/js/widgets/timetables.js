module.exports = function(Site, $) {
	
	Site.widget("timetable", {
		_create() {
			
			var self = this;
			
			this.element.find("img[alt*=access]").parents("td").addClass("accessibility-indicator");
			
			this.cells = this.element.find("td, th");
			this.rows = this.element.find("tr");
			
			this.rows.each(function() {
				$(this).children().each(function(k) {
					$(this).attr('colnum', k);
				});
			});
			
			this.element.find("td, th").mouseenter(function() {
				self.setActiveCell($(this));
			}).mouseleave(function() {
				self.setActiveCell(null);
			});
			
			this.initMobile();
			
		},
		setActiveCell: function(el) {
			
			this.rows.removeClass("hovering");
			this.cells.removeClass("hovering");
			
			if(el) {
				el.parents("tr").addClass("hovering");
				console.log("[colnum="+el.attr('colnum')+"]");
				this.cells.filter("[colnum="+el.attr('colnum')+"]").addClass("hovering");
			}
			
		},
		initMobile: function() {
			
			//$('#rightcolumn').remove();
			//$('#contentcolumn').removeClass('twocolumn');
			$('<div id="NewTable"></div>').insertAfter(this.element);
			
			
			$(window).resize(function(){
			
			if($(window).width()<=924)
			{
			   $("#NewTable").css("display","block");
			   $("table.timetable").css('display','none');   
			   $("#wheel_note").css('display','none');  
			}
			else
			{
			   $("#NewTable").css("display","none");
			   $("table.timetable").css('display',''); 
			   $("#wheel_note").css('display','block');  
			 
			}
			
			  if($(window).width()<=752)
			  {
			 //        $('#navcontainer').insertAfter($('#contentcontainer'));
			  }
			  else
			  {
			 //        $('#navcontainer').insertAfter($('#headercontainer'));
			  }
			
			});
			
			
			
			
			// JavaScript Document
			if($(window).width()<=924)
			{
			   $("#NewTable").css("display","block");
			   $("table.timetable").css('display','none');   
			   $("#wheel_note").css('display','none');  
			}
			else
			{
			   $("#NewTable").css("display","none");
			   $("table.timetable").css('display','');
			   $("#wheel_note").css('display','block');  
			 
			
			   setTimeout(function() { window.scrollTo(0, 1) }, 100);
			}
				
			// Global variables declaration
				var myTableArray = [];
				var CurrentLoc = window.location;
				var image = $('<img />').attr('src', 'https://www.action.act.gov.au/__data/assets/image/0019/340255/back.png');
			
			// START - Show all times
			function FullTable() {
				
				$('#NewTable').empty();	
			//	$("#route_direction").css('display','block');	
				myTableArray [0] [0] = "Route";		
				myTableArray [0] [1] = "*";	
				
				// TABLE
				var table = $('<table></table>').attr('id', 'ResultTable');
				// Header
				$('<tr></tr>').addClass('TableHeader').addClass('TableTopHeader').html('<td>&nbsp;</td><td>&nbsp;</td><td>START</td><td>END</td><td>&nbsp;</td>').appendTo(table);
			
				for (let row = 0; row < myTableArray . length; ++ row) 
				{ 
					var text_row = $('<tr></tr>');
					var col; 
					//	for (col = 0; col < myTableArray [row] . length; ++ col) 
					// add cycle for all columns
					$('<td></td>').addClass('RouteTime').html(myTableArray [row] [0]).appendTo(text_row);
					$('<td></td>').addClass('').html(myTableArray [row] [1]).appendTo(text_row);
					$('<td></td>').addClass('RouteTime').html(myTableArray [row] [2]).appendTo(text_row);
					$('<td></td>').addClass('RouteTime').html(myTableArray [row] [myTableArray [row] . length-1]).appendTo(text_row);		
					// end of cylce	
			
					// No expand for header		
			   		if (row>0)  {
						let link = $('<a href="#" class="mobile-timetable-expander"><span class="accessibility-hidden">Expand Route</span></a>');
						link.click(function(e) {
							ChooseRow(row);
							e.preventDefault();
						});
				   		$('<td></td>').addClass('RouteLink').html(link).appendTo(text_row);
			   		}
			   		else
			   		{
			   	   		text_row.addClass('TableHeader');
				    	$('<td></td>').addClass('RouteLink').html('Expand Route').appendTo(text_row);
			   		}
			   		text_row.appendTo(table);
				} 
				
				// Wheelchair Accessible note
				$('<tr></tr>').addClass('TableHeader').addClass('TableTopHeader').html('<td colspan="5">* &nbsp;<img width="36" height="36" src="https://www.action.act.gov.au/__data/assets/image/0020/340256/easyaccess_icon.png" alt="wheelchair" style="vertical-align: middle;">&nbsp;&nbsp;- ACTION’s fleet includes Easy Access, wheelchair accessible buses. Every effort is made to ensure an Easy Access bus operates at the times shown, however sometimes due to unforeseen operational circumstances an Easy Access bus may be replaced with a standard bus.</td>').appendTo(table);
			
				table.appendTo('#NewTable');
				// $("#ResultTable tr:odd").css("background-color", "#DDDDDD");
				// Go to the header
				
			if($(window).width()<=1024) {
				setTimeout(function() { window.scrollTo(0, 1) }, 100);}
			else {
			   setTimeout(function() { window.scrollTo(0, 1) }, 100);
			}
			
			
			}
			// END - Show all times
			
			
			// START - Show only one route
			function ChooseRow(trigger) {
			
				$('#NewTable').empty();	
			//	$("#route_direction").css('display','none');		
				myTableArray [0] [1] = "Wheelchair Accessible*";	
				
				// TABLE
				var table = $('<table></table>').attr('id', 'ResultRouteTable');
				// Header
			   	$('<tr></tr>').addClass('TableHeader').html(' <td>Route <strong>' + myTableArray [trigger] [0] + '</strong><br />Stop Description</td><td><a class="mobile-timetable-collapse-route">Back</a></td>').appendTo(table);
			
				var col; 
				for (col = 1; col < myTableArray [trigger] . length; ++ col) 
				{ 
					$('<tr></tr>').html(' <td>' + myTableArray [0] [col] + '</td><td>' + myTableArray [trigger] [col] + '</td>').appendTo(table);
				} 
			
				table.appendTo('#NewTable');
				$("#ResultRouteTable tr:odd").addClass("odd");
				
				table.find(".mobile-timetable-collapse-route").click(FullTable);
			
				// Go to the table  
			if($(window).width()<=1024)
			{
				setTimeout(function() { window.scrollTo(0, 1) }, 100);}
			else
			{
			   setTimeout(function() { window.scrollTo(0, 1) }, 100);
			}
			
			}
			// END - Show only one route
			
			
			
			
			
			
			// START - array populating
			$("table.timetable tr").each(function() {
			    var arrayOfThisRow = [];
			// header	
			    var headerData = $(this).find('th');
			    if (headerData.length > 0) {
			        headerData.each(function() { arrayOfThisRow.push($(this).text()); });
			        myTableArray.push(arrayOfThisRow);
			    }
			// body		
			    var tableData = $(this).find('td');
			    if (tableData.length > 0) {
			        tableData.each(function() { arrayOfThisRow.push($(this).html()); });
			        myTableArray.push(arrayOfThisRow);
			    }
				
			});
			// END - array populating
			
			myTableArray [0] [1] = " ";
			FullTable();

		}
	});
	
	Site.widget('routeSearch', {
		_create() {
			this.button = this.element.find(".button, button");
			this.input = this.element.find("input").autocompleteMenu({
				mode: 'input',
				hasIcons: true,
				cacheSpace: 'location',
				getItems: (query, wasClicked, callback) => {
					this.loadAndSearch(query, callback);
				},
				onChange: (item) => {
					console.log("Selected", item);
					this.selectedItem = item;
					this.updateButton();
				}
			});
			this.updateButton();
			
			this.input.on('keydown', (e) => {
				// if(e.keyCode == 13) this.submit();
			});
			this.button.click(() => {
				this.submit();
			});
		},
		submit() {
			if(this.selectedItem && !this.selectedItem.manuallyEntered) {
				window.location.href = this.selectedItem.url;
			}
		},
		loadAndSearch(query, callback) {
			this.ensureLoaded((data) => {
				let queryParts = query.toLowerCase().split(' ');
				let output = [];
				for(let item of data) {
					for(let term of queryParts) {
						if(item.searchIndex.indexOf(term) >= 0) {
							output.push(item);
							break;
						}
					}
				}
				callback(null, output.slice(0, 10));
			});
		},
		ensureLoaded(callback) {
			if(this.loaded) {
				callback(this.data);
			} else {
				$.get(Site.config.routesByNumberXMLURL, (response) => {
					let output = [];
					$(response).find("routename").each(function() {
						let el = $(this);
						output.push({
							label: el.find("number").text(),
							searchIndex: el.find("number").text().toLowerCase(),
							url: el.find("url").text(),
							icon: 'transit-stop'
						});
					});
					this.data = output;
					console.log("Output is", output);
					callback(this.data);
				});
			}
		},
		updateButton() {
			if(this.selectedItem && !this.selectedItem.manuallyEntered) {
				this.button.removeClass('disabled').prop('disabled', false);
			} else {
				this.button.addClass('disabled').prop('disabled', true);
			}
		}
	});
};