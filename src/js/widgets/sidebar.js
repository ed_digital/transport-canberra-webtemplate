module.exports = (Site, $) => {
	
	Site.widget('sidebarAccordian', {
		_create() {
			
			this.element.addClass("accordian")
			this.element.find("h3").click(() => {
				this.element.toggleClass('accordian-active');
			});
			
		}
	});
	
};