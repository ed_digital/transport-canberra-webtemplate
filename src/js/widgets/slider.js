import KeyCode from '../keycodes';

module.exports = (Site, $) => {
	
	Site.widget('imageSlider', {
		options: {
			duration: 500
		},
		_create: function() {
			
			this.slides = this.element.find(".slide");
			
			this.currentIndex = 0;
			
			$(document).keydown((e) => {
				if(!e.target || e.target === document.body) {
					if(e.keyCode == KeyCode.LEFT) {
						this.go(-1);
					} else if(e.keyCode == KeyCode.RIGHT) {
						this.go(+1);
					}
					e.preventDefault();
				}
			});
			
			// Add indicators
			this.indicatorsEl = this.element.find(".indicators");
			this.slides.each((index) => {
				
				let item = $("<span class='item'></span>").appendTo(this.indicatorsEl);
				item.click(() => {
					this.goToIndex(index, this.currentIndex - index > 0 ? -1 : 1);
				});
				
			});
			this.slides.first().addClass('active');
			this.indicators = this.indicatorsEl.children();
			this.indicators.first().addClass('active');
			
			this.getSlideAnimator(this.slides.first()).fadeIn();
			
			this.play();
			
			this.element.mouseenter(() => this.pause());
			this.element.mouseleave(() => this.play());
			
		},
		pause: function() {
			clearTimeout(this.playTimer);
		},
		play: function() {
			clearTimeout(this.playTimer);
			this.playTimer = setInterval(() => {
				this.go(1);
			}, 5000);
		},
		go: function(dir) {
			
			let index = this.currentIndex + dir;
			if(index < 0) {
				index = this.slides.length - 1;
			}
			if(index >= this.slides.length) {
				index = 0;
			}
			
			this.goToIndex(index, dir);
			
			
		},
		getSlideAnimator: function(el) {
		
			let children = [
				el.find('.image'),
				el.find('.modes'),
				el.find('h3'),
				el.find('.description'),
				el.find('.more')
			];
			
			let outDuration = 400;
			let inDuration = 1000;
			let inItemDuration = 500;
			let stepGap = (inDuration - inItemDuration) / children.length;
			
			let instant = (opacity) => {
				for(let child of children) {
					child.stop().css('opacity', 0);
				}
			};
			
			let isIE = $('html').hasClass('lt-ie9');
			
			return {
				el: el,
				fadeOut(cb) {
					if(isIE) {
						el.animate({opacity: 0}, {
							duration: outDuration,
							complete: () => {
								el.css('display', 'none');
								if(cb) cb();
							}
						});
					} else {
						el.transition({
							opacity: 0
						}, {
							duration: outDuration,
							complete: () => {
								instant(1);
								el.hide();
								if(cb) cb();
							}
						});
					}
				},
				fadeIn() {
					if(isIE) {
						el.css({'opacity':0, 'display': 'block'}).animate({
							opacity: 1
						}, {
							duration: outDuration
						});
					} else {
						instant(0);
						el.css({display: 'block', opacity: 1});
						for(let k in children) {
							let item = children[k];
							item.delay(stepGap * k).transition({
								opacity: 1
							}, {
								duration: inItemDuration
							});
						}
					}
				}
			};
		},
		goToIndex: function(newIndex, dir) {
			
			if(this.disabled) return;
			if(newIndex === this.currentIndex) return;
			
			this.disabled = true;
			setTimeout(() => {
				this.disabled = false;
			}, this.options.duration + 10);
			
			let lastSlide = this.getSlideAnimator(this.slides.eq(this.currentIndex));
			let nextSlide = this.getSlideAnimator(this.slides.eq(newIndex));
			
			lastSlide.fadeOut(nextSlide.fadeIn);
			
			// Update indicators and state
			this.indicators.removeClass('active').eq(newIndex).addClass('active');
			this.currentIndex = newIndex;
			
		},
		// goToIndex: function(newIndex, dir) {
		// 	
		// 	if(this.disabled) return;
		// 	if(newIndex === this.currentIndex) return;
		// 	
		// 	this.disabled = true;
		// 	setTimeout(() => {
		// 		this.disabled = false;
		// 	}, this.options.duration + 10);
		// 	
		// 	this.slides.each((k, _el) => {
		// 		let el = $(_el);
		// 		let start = '';
		// 		let end = '';
		// 		let visible = false;
		// 		if(k == newIndex) {
		// 			visible = true;
		// 			if(dir == -1) {
		// 				start = '-100%';
		// 			} else {
		// 				start = '100%';
		// 			}
		// 			end = '0%';
		// 		} else if(k == this.currentIndex) {
		// 			visible = true;
		// 			start = '0%';
		// 			if(dir == -1) {
		// 				end = '100%';
		// 			} else {
		// 				end = '-100%';
		// 			}
		// 		}
		// 		if(visible) {
		// 			el.css('left', start).stop().transition({
		// 				left: end
		// 			}, {
		// 				duration: this.options.duration
		// 			});
		// 		}
		// 	});
		// 	
		// 	this.indicators.removeClass('active').eq(newIndex).addClass('active');
		// 	
		// 	this.currentIndex = newIndex;
		// 	
		// }
	});
	
};