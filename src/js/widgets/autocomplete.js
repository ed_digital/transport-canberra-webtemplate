import KeyCode from '../keycodes';

module.exports = (Site, $) => {
	
	let autocompleteID = 0;
	
	let sharedCache = {
		
	};
	
	Site.widget('autocompleteMenu', {
		options: {
			mode: 'input',								// Either 'input' or 'select'
			label: null,		// Supply a jQuery element, only in 'select' mode
			labelText: "Choose an item",
			onSelect: function(item) {},
			onChange: function(item) {},
			onEnter: function() {},
			cacheSpace: 'default',
			hasIcons: false,
			getItems: function(query, callback) { },
			debounceTime: 400
		},
		_create() {
			
			this.reqID = 0;
			
			// Grab cacher
			if(!sharedCache[this.options.cacheSpace]) {
				sharedCache[this.options.cacheSpace] = {
					labelsToItems: {}
				};
			}
			this.cache = sharedCache[this.options.cacheSpace];
			
			if(this.options.mode === 'input') {
				// Use the current element
				this.listener = this.element;
			} else if(this.options.mode == 'select') {
				// Use a hidden element
				this.listener = $("<input class='invisible'>").appendTo(this.element);
			} else {
				throw new Error("Invalid autocompleteMenu mode");
			}
			
			// Accept arrow keys
			this.listener.on('keydown', (e) => {
				this.reqID++;
				if(e.keyCode == KeyCode.UP) {
					this.changeSelection(null, -1);
					e.preventDefault();
				} else if(e.keyCode == KeyCode.DOWN) {
					this.changeSelection(null, 1);
					e.preventDefault();
				} else if(e.keyCode == KeyCode.ENTER) {
					let menuVisible = this.menuEl.is(':visible');
					if(menuVisible) {
						// Maybe we have an active item?
						let activeItem = this.items.filter('.active');
						if(activeItem.size()) {
							this.didSelectItem(activeItem.data('item'), true, false);
						}
						this.hide();
						e.preventDefault();
						e.stopPropagation();
					}
				}
			});
			
			this.listener.on('focus', () => {
				this.focused = true; 
				this.element.addClass('focused');
				this.focusTime = new Date().getTime();
			}).on('blur', () => {
				this.focused = false;
				this.focusTime = null;
				this.element.removeClass('focused');
				this.hide();
			});
			
			this.listener.on('click', (e) => {
				if(!this.focusTime || new Date().getTime() - this.focusTime < 100) {
					if(e.target.select) {
						e.target.select();
						this.refreshList(e.target.value, true);
					}
				}
			});
			
			if(this.options.mode == 'input') {
				let lastValue;
				this.listener.on('change keyup keydown', (e) => {
					if(e.keyCode === KeyCode.ENTER || e.keyCode === KeyCode.UP || e.keyCode === KeyCode.DOWN) return;
					let value = this.element.val();
					this.refreshList(value);
					if(value && value in this.cache.labelsToItems) {
						this.didSelectItem(this.cache.labelsToItems[value], true, true);
					} else {
						this.didSelectItem({
							label: value,
							manuallyEntered: true
						}, true, true);
					}
				});
			} else if(this.options.mode == 'select') {
				this.options.label.click((e) => {
					this.listener.focus();
				});
				this.listener.focus(() => {
					this.refreshList(null, true);
				});
				this.listener.attr('id', 'ac_'+(++autocompleteID));
				$("<label class='accessibility-hidden' for='ac_"+autocompleteID+"'>"+this.options.labelText+"</label>").insertAfter(this.listener);
			}
			
			this.menuEl = $("<div class='autocomplete-menu'></div>").insertAfter(this.element);
			this.itemsEl = $("<ul class='items'></ul>").appendTo(this.menuEl);
			this.items = this.itemsEl.children("li");
			
			if(this.options.hasIcons) {
				this.menuEl.addClass('has-icons');
			}
			
		},
		setText(text) {
			if(this.options.mode == 'select') {
				this.options.label.text(text);
			} else if(this.options.mode == 'input') {
				this.listener.val(text);
			}
		},
		setItem(item) {
			this.didSelectItem(item, true, false);
		},
		show() {
			this.menuEl.show();
		},
		hide() {
			this.menuEl.hide();
			this.itemsEl.empty();
		},
		_refresh(query, clicked) {
			let reqID = ++this.reqID;
			this.options.getItems(query, clicked, (err, items) => {
				if(reqID === this.reqID) {
					if(err) {
						console.error("Autocomplete Error: ",err);
						this.hide();
					} else {
						if(this.focused) {
							this.show();
							this.populateList(items);
						}
					}
				}
			});
		},
		refreshList(query, clicked) {
			let action = () => {
				this._refresh(query, clicked);
				this.lastRefreshTime = new Date().getTime();
				clearTimeout(this._refreshDebouncer);
				this._refreshDebouncer = null;
			};
			clearTimeout(this._refreshDebouncer);
			if(!this.lastRefreshTime) {
				this.lastRefreshTime = new Date().getTime();
			}
			if(this._refreshDebouncer && new Date().getTime() - this.lastRefreshTime > 1000) {
				action();
			} else {
				this._refreshDebouncer = setTimeout(action, this.options.debounceTime);
			}
		},
		changeSelection(index, direction) {
			
			if(index === null) {
				index = -1;
				this.items.each((k, el) => {
					if(el.className.indexOf('active') > -1) {
						index = k;
					}
				});
			}
			
			if(typeof index !== 'number') {
				index = -1;
			}
			
			if(direction) {
				index += direction;
			}
			
			this.items.removeClass('active');
			
			let selectedItemEl = this.items.eq(index);
			selectedItemEl.addClass('active');
			
			this.didSelectItem(selectedItemEl.data('item'), false);
			
		},
		populateList(items) {
			
			this.itemsEl.empty();
			
			const addItem = (index, item) => {
				let itemEl = $("<li></li>").appendTo(this.itemsEl);
				itemEl.text(item.label);
				itemEl.attr('icon', item.icon);
				itemEl.data('item', item);
				itemEl.mouseenter(() => {
					this.changeSelection(index);
				}).mousedown((e) => {
					this.didSelectItem(item, true);
					this.hide();
					e.preventDefault();
					e.stopPropagation();
				});
			};
			
			for(let index = 0; index < items.length; index++) {
				let item = items[index];
				this.cache.labelsToItems[item.label] = item;
				addItem(index, item);
			}
			
			this.items = this.itemsEl.children("li");
			
		},
		didSelectItem(item, confirmed, wasTyped) {
			if(confirmed && !wasTyped) {
				this.setText(item.label);
			}
			if(confirmed && this.options.onChange) {
				this.options.onChange(item);
			} else if(this.options.onSelect) {
				this.options.onSelect(item);
			}
		}
	});
};