module.exports = (Site, $) => {
	
	Site.widget('tabs', {
		options: {
			
		},
		_create() {
			
			this.tabs = this.element.find('.tab');
			
			this.setTab(this.tabs.first());
			
			this.tabs.each((k, el) => {
				let tab = $(el);
				tab.find('.tab-label').click((e) => {
					e.preventDefault();
					this.setTab(tab);
				});
			});
			
		},
		setTab(tab) {
			
			this.tabs.removeClass('active');
			tab.addClass('active');
			
		}
	});
	
};