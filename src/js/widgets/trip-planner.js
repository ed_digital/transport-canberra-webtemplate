import GoogleMapsLoader from 'google-maps';
import async from 'async';
import moment from 'moment';
import canberra from '../canberra';
import KeyCode from '../keycodes';
import _timeEntry from '../lib/jquery.timeentry.min.js';
import otherAutocompleteItems from '../static-autocomplete';

module.exports = (Site, $) => {
	
	Site.widget('tripPlanner', {
		_options: {
			
		},
		_create() {
			
			this._getGoogle();
			
			this.initFields();
			
		},
		initFields() {
			
			const fields = this.fields = {};
			
			fields.origin = this.element.find("input[name=origin]");
			fields.destination = this.element.find("input[name=destination]");
			fields.timeTypeContainer = this.element.find(".time-query-type");
			fields.dateTimeContainer = this.element.find(".date-time");
			fields.dateContainer = this.element.find(".date-time .date");
			fields.timeContainer = this.element.find(".date-time .time");
			fields.timeInput = $("<input type='text' id='tp_time' autocomplete='off'>").appendTo(fields.timeContainer);
			$("<label class='accessibility-hidden' for='tp_time'>Choose a time</label>").appendTo(fields.timeContainer);
			
			let state = this.state = {
				origin: null,
				destination: null,
				timeMode: 'now',
				date: new Date()
			};
			
			let stateChanged = () => {
				
				// State changed wooh. This doesn't do anything yet.
				let timeDisabled = false;
				if(state.mode == "walk" || state.mode == "bike") {
					timeDisabled = true;
					fields.timeTypeContainer.addClass("disabled").find("input, select").prop("disabled", true);
				} else {
					fields.timeTypeContainer.removeClass("disabled").find("input, select").prop("disabled", false);
				}
				
				if(state.timeMode === 'now') {
					timeDisabled = true;
				}
				
				if(timeDisabled) {
					fields.dateTimeContainer.addClass("disabled").find("input, select").prop("disabled", true);
				} else {
					fields.dateTimeContainer.removeClass("disabled").find("input, select").prop("disabled", false);
				}
				
			};
			
			// Origin and destination autocomplete
			fields.origin.autocompleteMenu({
				mode: 'input',
				hasIcons: true,
				cacheSpace: 'location',
				getItems: (query, wasClicked, callback) => {
					this.getLocationAutocomplete(query, wasClicked, callback);
				},
				onChange: (item) => {
					state.origin = item;
					this.ensureLatLng(item, () => {
						if(item !== state.origin) return;
						if(item.pending) {
							fields.origin.addClass('pending');
						} else {
							fields.origin.removeClass('pending');
						}
						stateChanged();
					});
					fields.origin.parent().attr('icon', item.icon);
					stateChanged();
				}
			});
			fields.destination.autocompleteMenu({
				mode: 'input',
				hasIcons: true,
				cacheSpace: 'location',
				getItems: (query, wasClicked, callback) => {
					this.getLocationAutocomplete(query, wasClicked, callback);
				},
				onChange: (item) => {
					state.destination = item;
					this.ensureLatLng(item, () => {
						if(item !== state.destination) return;
						if(item.pending) {
							fields.destination.addClass('pending');
						} else {
							fields.destination.removeClass('pending');
						}
						stateChanged();
					});
					fields.destination.parent().attr('icon', item.icon);
					stateChanged();
				}
			});
			
			// 'My Location' items
			this.element.find(".location-picker").each((k, el) => {
				el = $(el);
				let input = el.find("input");
				let trigger = $("<span class='use-current-location'></span>").appendTo(el);
				trigger.click(() => {
					input.autocompleteMenu('setItem', {
						label: 'My Location',
						icon: 'gps'
					});
				});
			});
			
			// Time type (Leave Now, Depart at, Arrive by)
			// let dateTimeHeight = fields.dateTimeContainer.height();
			fields.timeTypeContainer.autocompleteMenu({
				mode: 'select',
				labelText: "Select a date",
				label: $("<span class='faux-select'>Leave now</span>").appendTo(fields.timeTypeContainer),
				debounceTime: 0,
				getItems: (items, clicked, callback) => {
					callback(null, [
						{
							label: "Leave now",
							value: "now"
						},
						{
							label: "Depart at...",
							value: "departure"
						},
						{
							label: "Arrive by...",
							value: "arrival"
						}
					]);
				},
				onChange: (item) => {
					state.timeMode = item.value;
					
					if(item.value !== 'now' && !fields.timeInput.val()) {
						// Set time to current time + 5 mins
						fields.timeInput.timeEntry('setTime', new Date());
					}
					
					
										
					// if(item.value === 'now') {
					// 	fields.dateTimeContainer.transition({
					// 		opacity: 0,
					// 		marginTop: -dateTimeHeight
					// 	}, {
					// 		complete: function() {
					// 			fields.dateTimeContainer.css('display', 'none');
					// 		}
					// 	});
					// } else {
					// 	fields.dateTimeContainer.css('display', 'block').transition({
					// 		opacity: 1,
					// 		marginTop: 10
					// 	});
					// }
					stateChanged();
				}
			});
			
			// fields.dateTimeContainer.css({
			// 	opacity: 0,
			// 	display: 'none',
			// 	marginTop: -dateTimeHeight
			// });
			
			// Date
			fields.dateContainer.autocompleteMenu({
				mode: 'select',
				label: $("<span class='faux-select'>Today</span>").appendTo(fields.dateContainer),
				debounceTime: 0,
				getItems: (query, clicked, callback) => {
					let dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
					let monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
					let totalDays = 6;
					let items = [];
					items.push({
						label: 'Today',
						value: new Date()
					});
					for(let d = 1; d < totalDays; d++) {
						let date = new Date(new Date().getTime() + d*24*60*60*1000);
						items.push({
							label: dayNames[date.getDay()]+' '+monthNames[date.getMonth()]+' '+date.getDate(),
							value: date
						});
					}
					callback(null, items);
				},
				onChange: (item) => {
					state.date = item.value;
					stateChanged();
				}
			});
			
			// Time
			fields.timeInput.timeEntry({
				spinnerImage: null
			}).on('change', function() {
				state.time = this.value;
				stateChanged();
			});
			
			fields.dateTimeContainer.addClass("disabled").find("input, select").prop("disabled", true);
			fields.timeInput.timeEntry('setTime', new Date());
			
			// Mode of transport
			fields.modes = this.element.find("input[name=travelMode]").on('change', function() {
				if(this.checked) {
					state.mode = this.value;
				}
				stateChanged();
			});
			
			// Submit button
			$(".submit .button").click((e) => {
				e.preventDefault();
				
				// ERRORs
				if(!state.origin || !state.destination) return;
				
				let args = {
					trip: [
						{
							text: state.origin.fullSearchTerm || state.origin.label,
							lat: state.origin.lat,
							lng: state.origin.lng
						},
						{
							text: state.destination.fullSearchTerm || state.destination.label,
							lat: state.destination.lat,
							lng: state.destination.lng
						}
					],
					mode: state.mode,
					timing: state.timeMode,
					time: (state.time && state.date) ? moment(moment(state.date).format('YYYY-M-D') + ' ' + state.time, 'YYYY-M-D hh:mm:A') : undefined
				};
				
				this.remember(this.state);
				
				let url = this.generateMapsURL(args);
				window.open(url, 'directions');
			});
			
			this.restore();
			
		},
		remember(state) {
			
			if(localStorage && JSON) {
				if(state.origin) {
					localStorage.setItem('previous.origin', JSON.stringify(state.origin));
				}
				if(state.destination) {
					localStorage.setItem('previous.destination', JSON.stringify(state.destination));
				}
			}
			
		},
		restore() {
			
			if(localStorage && JSON) {
				let origin, destination;
				try {
					origin = JSON.parse(localStorage.getItem('previous.origin'));
					destination = JSON.parse(localStorage.getItem('previous.destination'));
				} catch(e) {
					return;
				}
				if(origin) {
					origin.icon = 'history';
					this.fields.origin.autocompleteMenu('setItem', origin);
				}
				if(destination) {
					destination.icon = 'history';
					this.fields.destination.autocompleteMenu('setItem', destination);
				}
			}
			
		},
		_getGoogle(callback) {
			GoogleMapsLoader.LIBRARIES = ['places'];
			GoogleMapsLoader.KEY = Site.config.googleKey;
			GoogleMapsLoader.load((google) => {
				
				if(!this.googleAutocomplete) {
					this.googleAutocomplete = new google.maps.places.AutocompleteService();
				}
				
				if(!this.googlePlaces) {
					this.googlePlaces = new google.maps.places.PlacesService($("<div>asdfasdf</div>").appendTo(document.body)[0]);
				}
				
				if(callback) callback(google);
				
			});
		},
		/*
			eg {
				trip: [
					{
						text: '10/19 Sloane Street',
						lat: 151.1401601,
						lng: -33.8896253
					},
					{
						text: 'Work',
						lat: 151.2004887,
						lng: -33.8983442
					}
				],
				mode: 'public',			// public, walk, bike, drive
				timing: 'arrival',			// now, departure, arrival
				time: 'Tue Jun 27 2016 2:10:55 GMT+1000 (AEST)'//.getTime() - new Date().getTimezoneOffset() * 60000,
			};
		*/
		generateMapsURL(args) {
			
			let directionsPath = [];
			let data = [];
			
			// Add each journey segment
			if(args.trip) {
				for(let item of args.trip) {
					if(item.text && item.text.match(/(my|current)\ location/i)) {
						directionsPath.push("current+location");
					} else {
						let segmentName = encodeURIComponent(item.text).replace(/\%20/g,'+');
						directionsPath.push(segmentName);
					}
					// Add a journey segment, if there is lat/lng
					if(item.lat && item.lng) {
						data.push('!1m3');
						data.push('!2m2');
						data.push('!1d'+item.lng);
						data.push('!2d'+item.lat);
					} else {
						//data.push('!1m0');
						data.push('!1m0');
					}
				}
			}

			// Transit mode?
			let modality = 3;
			if(args.mode === 'walk') {
				modality = 2;
			} else if(args.mode === 'bike') {
				modality = 1;
			} else if(args.mode === 'drive') {
				modality = 0;
			}
			data.push('!3e'+modality);

			// Timing
			let timeType = null;
			if(args.timing === 'departure') {
				timeType = 0;
			} else if(args.timing === 'arrival') {
				timeType = 1;
			}
			if(timeType !== null) {
				data.push('!2m3');
				data.push('!6e'+timeType);
				data.push('!7e2');
				//let offset = new Date().getTimezoneOffset() * 60000;
				//let time = (new Date(args.time).getTime() / 1000) + offset;
				window.moment = moment;
				//let time = moment(args.time).unix() + new Date().getTimezoneOffset();
				//let offset = new Date().getTimezoneOffset() * 60000;
				//let time = (new Date(args.time).getTime() / 1000) + offset;
				let time = moment(args.time).unix() - new Date().getTimezoneOffset() * 60;
				data.push('!8j'+time);
			}
				
			// Directions header
			data.unshift('!4m'+data.length);
			data.unshift('!4m'+data.length);
			data.unshift('!4b1');
			
			window.url = 'https://www.google.com.au/maps/dir/'+directionsPath.join('/')+'/data='+data.join('');

			return 'https://www.google.com.au/maps/dir/'+directionsPath.join('/')+'/data='+data.join('');
		},
		getLocationAutocomplete(query, includeExtras, callback) {
			
			this._getGoogle(() => {
				
				let output = [];
				let failed = false;
				
				async.parallel([
					// Other Autocomplete Items
					(next) => {
						let words = query.toLowerCase().split(' ');
						if(words.length > 0) {
							for(let item of otherAutocompleteItems) {
								let fail = false;
								for(let word of words) {
									if(item.searchIndex.indexOf(word) === -1) {
										fail = true;
										break;
									}
								}
								if(!fail) output.push(item);
							}
						}
						next();
					},
					// Autocomplete addresses and stuff
					(next) => {
						if(!query) return next();
						this.googleAutocomplete.getQueryPredictions({
							bounds: new google.maps.LatLngBounds(canberra.bounds.sw, canberra.bounds.ne),
							input: query
						}, (result, status) => {
							
							if(status !== 'OK') {
								failed = true;
								next();
								return;
							}
							
							// Filter out non-ACT results
							for(let item of result) {
								if(item.description.indexOf('Australian Capital Territory, Australia') > 0) {
									let icon = 'none';
									if(item.types && item.types.indexOf('transit_station') > -1) {
										icon = 'transit-stop';
									} else if(item.types && item.types.indexOf('establishment') > -1) {
										icon = 'establishment';
									}
									output.push({
										label: item.description.replace(", Australian Capital Territory, Australia", ""),
										placeID: item.place_id,
										resultType: "autocomplete",
										icon: icon
									});
								}
							}
							
							next();
							
						});
					},
					// Fetch bus stops
					// (next) => {
					// 	if(!query) return next();
					// 	let cleanedQuery = query.split(/\s+/).filter((segment) => {
					// 		if(segment.match(/^[A-Z]{4,50}$/i)) {
					// 			return true;
					// 		} else {
					// 			return false;
					// 		}
					// 	});
					// 	this.googlePlaces.textSearch({
					// 		bounds: new google.maps.LatLngBounds(canberra.bounds.sw, canberra.bounds.ne),
					// 		query: cleanedQuery,
					// 		type: 'transit_station'
					// 	}, (result, status) => {
					// 		if(status == 'OK') {
					// 			for(let item of result) {
					// 				output.push({
					// 					label: item.name,
					// 					placeID: item.placeID,
					// 					lat: item.geometry.location.lat(),
					// 					lng: item.geometry.location.lng(),
					// 					resultType: "autocomplete-bus-stop",
					// 					icon: "transit-stop"
					// 				});
					// 			}
					// 		}
					// 		next();
					// 	});
					// }
				], (next) => {
					callback(null, output);
				});
				
			});
			
		},
		ensureLatLng(item, changed) {
			if(!item.lat && !item.lng && item.placeID) {
				// Item has a placeID, but no lat/lng
				item.pending = true;
				changed();
				this.googlePlaces.getDetails({
					placeId: item.placeID
				}, (place, status) => {
					item.pending = false;
					if(status == google.maps.places.PlacesServiceStatus.OK && place && place.geometry && place.geometry.location) {
						item.lat = place.geometry.location.lat();
						item.lng = place.geometry.location.lng();
					}
					changed();
				});
			}
		}
	});
	
};