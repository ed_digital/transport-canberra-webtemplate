module.exports = function(Site, $) {
	
	Site.widget('mobileNav', {
		_create() {
			
			this.toggle = $("<div class='mobile-nav-toggle'><span class='label'>Menu</span> <span class='icon'><span></span></span></div>").appendTo(this.element);
			this.dropdown = $("<div class='mobile-nav-panel'><div class='inner'></div></div>").appendTo(this.element);
			let inner = this.dropdown.find(".inner");
			
			// Search
			$("<div class='search'></div>").appendTo(inner).append($("#sitesearch").parent().html());
			
			// Menu items
			$("<div class='items'></div>").appendTo(inner).append($("#dropdownnav").html());
			
			this.element.click((e) => e.stopPropagation());
			
			this.toggle.click((e) => {
				$(document.body).toggleClass('nav-active');
			});
			
			$(document).click(() => $(document.body).removeClass('nav-active'));
		}
	});
	
};