module.exports = function(Site, $) {
	
	Site.widget("tc", {
		_create() {
			
			// Initialize timetables
			$(".timetable").timetable();
			
			$("#search-advanced-toggle a").click((e) => {
				e.preventDefault();
				$("#advsearchform").toggle();
			});
			
		}
	});
	
};