let items = [
	{
		label: "My Location",
		icon: "gps",
		keywords: "current location"
	},
	{
		label: "City Bus Station",
		keywords: "civic interchange",
		fullSearchTerm: "City Station",
		icon: "transit-stop",
		lat: -35.278556,
		lng: 149.130169
	},
	{
		label: "Cohen Street Bus Station",
		keywords: "interchange",
		icon: "transit-stop",
		lat: -35.239981,
		lng: 149.059983
	},
	{
		label: "Belconnen Westfield Bus Station",
		keywords: "interchange",
		icon: "transit-stop",
		lat: -35.238780,
		lng: 149.063858
	},
	{
		label: "Belconnen Community Bus Station",
		keywords: "interchange",
		icon: "transit-stop",
		lat: -35.239786, 
		lng: 149.069172
	},
	{
		label: "Woden Bus Station",
		keywords: "interchange",
		icon: "transit-stop",
		lat: -35.344312,
		lng: 149.087665
	},
	{
		label: "Tuggeranong Bus Station",
		keywords: "interchange tuggers",
		icon: "transit-stop",
		lat: -35.414552,
		lng: 149.065511
	},
	{
		label: "Gungahlin Bus Station",
		keywords: "interchange gunghalin gunners",
		icon: "transit-stop",
		lat: -35.185242,
		lng: 149.133131
	}
];

for(let item of items) {
	item.searchIndex = (item.label+' '+item.keywords).toLowerCase();
}

module.exports = items;