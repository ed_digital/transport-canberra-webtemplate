# Transport Canberra

## Page List
* [Homepage](homepage.html)
* [Search Results](search.html)
* [Basic Subpage](page.html)
* [Tiles Sample](tiles-sample.html)
* [MyWay Test Page](myway.html)
* [News Listing Page](news.html)
* [Service Updates Listing Page](service-updates.html)
* [Routes & Timetables](routes.html)
* [Timetable Demo](timetable.html)

## Build System

* We've created a mini build system for development purposes, which takes source JS/LESS/Swig templates and compiles them to the `build` directory. It also serves as a development environment.
* JS is written in ES6, and is compiled to using [Babel](http://babljs.io), from `src/js` to `build/js`
* CSS is written in LESS, and compiled using the official LESS JS library from `src/less` to `build/css`
* For development purposes, we've used the [Swig](http://paularmstrong.github.io/swig/) templating engine to live-compile the templates into several pages (see the `src/pages` and `src/templates` and `build/pages` directories). This is useful for making sure everything fits well together under varying circumstances. 
These should also be useful for seeing how we see logic and data being used to generate markup.

To run the build system yourself, you'll need to install node.js v6, then you run `cd <path>`, `npm install` to install necessary dependencies, then `node start.js` to start the build/dev server on <a href="http://127.0.0.1:2016/">port 2016</a>. The project will be rebuilt immediately, and any changes to files within the `src` directory will trigger the rebuild process, as well as an automatic page refresh.

## Widget System
JavaScript event handlers and functionality are attached to DOM elements via jQuery widgets. Further widgets can be defined like so

	$.widget('tc.someWidget, {
		options: {
			alertText: "Hello"
		},
		_create: function() {
			var self = this;
			this.element.find('a').click(function() {
				alert(self.options.text);
			});
		}
	});

Then add the `data-widget='someWidget'` attribute to that element. When the page first loads, elements with a `data-widget` attribute will spawn a jQuery widget with that same name. You can also pass options by using other `data-*` attributes. Names are automatically converted from lower case with dashes, to camel case without dashes.

	<div data-widget="some-widget" data-alert-text="Oh hi there">
		<p>Click on the <a>thing</a></p>
	</div>

## General
* Tried to use the same HTML where possible, and just changing where necessary.
* Using our 12 column LESS-based grid system, which has some markup/class name requirements ie. `.wrapper > .row > .col-n`. Columns can also have child rows.
* Site max width is 1170px. At lower resolutions, the grid becomes percentage-based.
* Have used the original markup for top-level nav (dropdown), just customised with CSS.
* Have used the original markup for search box, except for the search button which now uses a `<button>` tag to allow us to use an icon instead of text, but keep it accessible to screen readers.
* Header logos markup has been rewritten
* Footer shares some of the original code (the standard footer links and fat-footer areas), however has been customised to incorporate our grid system and social media links/icons.
* Some pages have no sidebar (see `timetable.html`) — this is accomplished by removing the `.sidebar` element, but also requires different `col-n` classes on the `.main-content` element.

## Homepage
[View Page](homepage.html)

### Trip Planner
* JS based, using Google Maps Javascript API for autocompletion. Sends users to Google Map.
* Bus interchanges with aliases (eg. civic interchange) are in `/src/js/static-autocomplete.js`
* 

### Slider
* Completely rewritten HTML/CSS/JS
* Each slider item should be taggable with zero or more of the following: `bus`, `light-rail`, `active-travel`. This doesn't show up as text, but rather as icons, by adding empty `<span>` elements to the `.modes` container.
* Entire tile is a link, so that screen readers will read the title first

### News
* Displays a list of the latest 5 news articles
* Shows a list of categories for each article, next to the date. Category text should link to the 'News & Events' page, with that category pre-selected.

### Service Updates
* Exactly the same markup as on the actual Service Updates listing page, however should be limited to 5
* To display the correct icon (alert or information), use the classes `label-0` and `label-1` on the `.item` elements
* Show the Mode (ie. Light Rail/Buses) and the region (eg. Gungahlin, Belconnen etc) as links beside the date. These should link to the service updates page with that filter pre-selected.
* Service updates themselves don't necessary have their own page, however the text for the update may have embedded links (for linking to a news article or other page)

## Search Results Page
[View Page](search.html)

* Used unmodified HTML from ACTION website, with rewritten CSS

## Timetable
[View Page](timetable.html)

* Used HTML from ACTION website, completely unmodified
* Overrode the CSS and added custom JS for row/column hovering
* We've replaced the existing `easyaccess_icon.png` icon with a new icon called `accessible-service.png`. Hopefully this is ok.
* Took the inline JavaScript for mobile responsiveness that was embedded in the page and transferred it to the `timetable` widget
* Also added JS to replace the table hover code to the `timetable` widget

## Search Results
[View Page](search.html)

* Used HTML from ACTION website, completely unmodified
* Wrote new CSS

## Tiles
[View Tiles Sample](tiles-sample.html)

* For top-level pages with multiple child pages, please use the larger tiles (with descriptions)
* For second-level pages, please use the smaller tiles (no description)

## News & Events
[View News & Events](news.html)

* Filter by category/date on the left
* The date list should be limited to maybe the last 12 months, with an archive link if easy enough to achieve
* News articles should be paginated (10 per page)

## Info/Alert
You can see the 'info' message box on the [news and events page](news.swig), and the 'alert' message box on the [service updates page](service-updates.swig).


## Service Updates
[View Service Updates](service-updates.html)

* Same filtering functionality and markup as on the News & Events page, except by 'Area' rather than category.
* Uses the same listing markup as on the related homepage section.
* To display the correct icon (alert or information), use the classes `label-0` and `label-1` on the `.item` elements
* Show the Mode (ie. Light Rail/Buses) and the region (eg. Gungahlin, Belconnen etc) as links beside the date. These should link to the service updates page with that filter pre-selected.
* Service updates themselves don't necessary have their own page, however the text for the update may have embedded links (for linking to a news article or other page)